if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi

test -r /sw/bin/init.sh && . /sw/bin/init.sh
export PATH=/opt/local/bin:/usr/local/mysql/bin:/usr/local/bin:$PATH
export PATH=/usr/local/apache2/bin:$PATH
export PATH=.:~/bin:$PATH

#j2ee stuff
export PATH=$PATH:/usr/local/j2sdkee1.3.1/bin
export J2EE_HOME=/usr/local/j2sdkee1.3.1
#export CLASSPATH=.:${J2EE_HOME}/lib/j2ee.jar
 
#tomcat stuff
export TOMCAT_HOME=/usr/local/tomcat
export CATALINA_HOME=$TOMCAT_HOME

export CLICOLOR=1
#export LSCOLORS=ExFxCxDxBxegedabagacad
export LSCOLORS=gxBxhxDxfxhxhxhxhxcxcx
export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd


##
# Your previous /Users/ryan/.profile file was backed up as /Users/ryan/.profile.macports-saved_2010-06-08_at_19:21:58
##

# MacPorts Installer addition on 2010-06-08_at_19:21:58: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/ryan/.profile file was backed up as /Users/ryan/.profile.macports-saved_2010-06-08_at_19:22:25
##

# MacPorts Installer addition on 2010-06-08_at_19:22:25: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/ryan/.profile file was backed up as /Users/ryan/.profile.macports-saved_2014-05-21_at_20:43:08
##

# MacPorts Installer addition on 2014-05-21_at_20:43:08: adding an appropriate PATH variable for use with MacPorts.
export PATH=/opt/local/bin:/opt/local/sbin:$PATH
# Finished adapting your PATH environment variable for use with MacPorts.


##
# Your previous /Users/ryan/.profile file was backed up as /Users/ryan/.profile.macports-saved_2015-02-28_at_18:01:46
##

# MacPorts Installer addition on 2015-02-28_at_18:01:46: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


#THIS MUST BE AT THE END OF THE FILE FOR GVM TO WORK!!!
[[ -s "/Users/ryan/.gvm/bin/gvm-init.sh" ]] && source "/Users/ryan/.gvm/bin/gvm-init.sh"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/ryan/.sdkman"
[[ -s "/Users/ryan/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/ryan/.sdkman/bin/sdkman-init.sh"

##
# Your previous /Users/ryan/.profile file was backed up as /Users/ryan/.profile.macports-saved_2016-07-10_at_21:04:17
##

# MacPorts Installer addition on 2016-07-10_at_21:04:17: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.


# MacPorts Installer addition on 2017-01-01_at_18:31:23: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

