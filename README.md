# dotfiles

Various dotfiles that may be useful across environments

## Vim

* https://octetz.com/docs/2020/2020-01-06-vim-k8s-yaml-support/
* https://github.com/junegunn/vim-plug/wiki/tips#automatic-installation

apt-get install nodejs ctags

Not all plugins are in source
in Vim: 
:PlugInstall
:CocInstall coc-yaml

Issue on ubuntu: https://github.com/neoclide/coc.nvim/issues/775

vim latest

 * https://itsfoss.com/vim-8-release-install/
`bash
sudo add-apt-repository ppa:jonathonf/vim
sudo apt update
sudo apt install vim
`