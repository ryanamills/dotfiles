
export DOCKER_CLIENT_TIMEOUT=120
export COMPOSE_HTTP_TIMEOUT=120
export ACTIVATOR_HOME=~/share/java/activator-dist-1.3.6
export CP=~/devel/689/trunk
export SAGE_PATH=~/devel/sage:~/devel/python/stocastic:~/share/python
export SAGE_LOAD_ATTACH_PATH=$SAGE_PATH
export GROOVY_HOME=/usr/local/opt/groovy/libexec

PATH=.:/usr/local/bin:~/bin:~/Dropbox/bin:$PATH:~/share/java/jython2.5.3:$CLIENT_HOME
export PATH
export PATH=/opt/subversion/bin:$PATH
export PATH=/Applications/Sage-4.7-OSX-64bit-10.6.app/Contents/Resources/sage:$PATH
export PATH=$ACTIVATOR_HOME:$PATH
export JENV_ROOT=/usr/local/var/jenv
export JAVA7_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_60.jdk/Contents/Home
export JAVA8_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_66.jdk/Contents/Home

#export JAVA_HOME=/System/Library/Frameworks/JavaVM.framework/Versions/CurrentJDK/Home
#export  JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
function setjdk() {
  if [ $# -ne 0 ]; then
   removeFromPath '/System/Library/Frameworks/JavaVM.framework/Home/bin'
   if [ -n "${JAVA_HOME+x}" ]; then
    removeFromPath $JAVA_HOME
   fi
   export JAVA_HOME=`/usr/libexec/java_home -v $@`
   export PATH=$JAVA_HOME/bin:$PATH
  fi
 }
 function removeFromPath() {
  export PATH=$(echo $PATH | sed -E -e "s;:$1;;" -e "s;$1:?;;")
 }
# see jenv
setjdk 1.7
export JBOSS_HOME=~/local/java/jboss-6.0.0.Final
export CLASSPATH=.:$JAVADIR/Classes/classes.jar:/usr/local/java/binding-1.1.2/binding-1.1.2.jar:/usr/local/java/binding-1.1.2/lib/forms-1.0.7.jar:/usr/local/java/jythonRelease_2_2alpha1/jython.jar

export CDPATH=./:~:~/Documents/workspace

#alias jython="rlwrap jython"
alias git_dev_to_master="git push origin && git checkout master && git pull && git pull develop && git push origin && git checkout develop"
alias lowercase="rename -f 'y/A-Z/a-z/'"
alias upercase="rename -f 'y/a-z/A-Z/'"
export CLIENT_HOME=/Applications/instantclient
#export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$CLIENT_HOME
export TNS_ADMIN=$CLIENT_HOME
export SQLPATH=$HOME/share/oracle
export PATH=$PATH:$CLIENT_HOME
export NLS_LANG=American_America.UTF8
alias sqlplus='rlwrap -b "" sqlplus'
#alias rm="echo Use 'del', or the full path '/bin/rm'"
alias trash='rmtrash'
alias del='rmtrash'
alias ls='gls --color'
export EDITOR=vi
export FXHISTORY=~/Documents/fxHistory
export FINANCIAL_VALUATION_DATA_INPUT=~/Documents/stockValuations/input
export FINANCIAL_VALUATION_DATA_OUTPUT=~/Documents/stockValuations/output
#export FINANCIAL_VALUATION_DATA_INPUT=~/Dropbox/Eclipse_Workspace/Finance/input
#export FINANCIAL_VALUATION_DATA_OUTPUT=~/Dropbox/Eclipse_Workspace/Finance/output
export GWT_HOME=~/share/java/GWT/gwt-2.7.0
export PYTHONPATH=${PYTHONPATH}:~/share/python
export PYTHONPATH=${PYTHONPATH}:~/Dropbox/Eclipse_Workspace/Finance/src
export PATH=$PATH:~/share/python

export PATH=$PATH:$GWT_HOME

export PATH=/Applications/SenchaSDKTools-2.0.0-Developer-Preview:$PATH

export PATH=/Applications/SenchaSDKTools-2.0.0-Developer-Preview/command:$PATH

export PATH=/Applications/SenchaSDKTools-2.0.0-Developer-Preview/jsbuilder:$PATH

export GRAILS_HOME=~/share/java/grails/grails-1.3.7
export PATH=$GRAILS_HOME/bin:$PATH
export APPENGINE_HOME=~/share/java/appengine-java-sdk-1.6.0
#MAHOUT STUFF
export MAHOUT_HOME=~/share/java/mahout-0.7
export HADOOP_HOME=/opt/local/share/java/hadoop-1.0.3
export PATH=$MAHOUT_HOME/bin:$HADOOP_HOME/bin:$PATH
export DERBY_HOME=~/share/java/db-derby-10.9.1.0-bin
export PATH=$DERBY_HOME/bin:$PATH
#export ANT_HOME=~/share/java/apache-ant-1.8.4
#export ANT_HOME=/usr/local/Cellar/ant/1.9.7
#complete -C $ANT_HOME/bin/complete-ant-cmd.pl ant
complete -C /usr/local/bin/complete-ant-cmd.pl ant
export PATH=$ANT_HOME/bin:$PATH
export PATH=$GRADLE_HOME/bin:$PATH
export PATH=$GRAILS_HOME/bin:$PATH
export GRAILS_OPTS="-Xmx1G -Xms256m -XX:MaxPermSize=256m"
export ANT_OPTS=" -Xms256M -Xmx512M -Xss10M"

# /etc/profile.d/complete-hosts.sh
# Autocomplete Hostnames for SSH etc.
# by Jean-Sebastien Morisset (http://surniaulula.com/)
_complete_hosts () {
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    host_list=`{
        for c in /etc/ssh_config /etc/ssh/ssh_config ~/.ssh/config
        do [ -r $c ] && sed -n -e 's/^Host[[:space:]]//p' -e 's/^[[:space:]]*HostName[[:space:]]//p' $c
        done
        for k in /etc/ssh_known_hosts /etc/ssh/ssh_known_hosts ~/.ssh/known_hosts
        do [ -r $k ] && egrep -v '^[#\[]' $k|cut -f 1 -d ' '|sed -e 's/[,:].*//g'
        done
        sed -n -e 's/^[0-9][0-9\.]*//p' /etc/hosts; }|tr ' ' '\n'|grep -v '*'`
    COMPREPLY=( $(compgen -W "${host_list}" -- $cur))
    return 0
}
complete -F _complete_hosts ssh
complete -F _complete_hosts host


export PATH=$PATH:~/Eclipse_Workspace/koan_edm/bin

server() {
   open "http://localhost:${1}" && python -m SimpleHTTPServer $1
}
export JAVA_OPTS="-Xmx1024M -XX:PermSize=512m"
export MAVEN_OPTS=$JAVA_OPTS
jetty(){
    java -jar ~/share/java/jetty-runner/jetty-runner-9.3.11.v20160721.jar $@
}
webapp(){
    java -jar ~/share/java/webapp-runner/webapp-runner.jar $@
}
export KOAN_SQL_HOME=/Applications/koan-sql
export PATH=$PATH:$KOAN_SQL_HOME/bin
export BRJS_HOME=~/share/java/BladeRunnerJS
export PATH=$BRJS_HOME/sdk:$PATH
brjs-old(){
    cd $BRJS_HOME/sdk
    ./brjs $@
}

#THIS MUST BE AT THE END OF THE FILE FOR GVM TO WORK!!!
export PATH="$HOME/.jenv/bin:$PATH"
#eval "$(jenv init -)"
#jenv global 1.7

function title {
    echo -ne "\033]0;"$*"\007"
}

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/ryan/.sdkman"
[[ -s "/Users/ryan/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/ryan/.sdkman/bin/sdkman-init.sh"
#https://gist.github.com/Ea87/46401a96df31cd208a87
. ~/.gradle-tab-completion.bash

#wildcard file . files
shopt -s dotglob
